package com.lufick.javatest;

import java.util.ArrayList;

/**
 * Created by  gulsh on 1/12/2019.
 */
public class EventMSG {

    public static class MSGStr{
        String msg;
        ArrayList<String> names;

        public MSGStr(String msg, ArrayList<String> names) {
            this.msg = msg;
            this.names = names;
        }
    }
}
