package com.lufick.javatest;

/**
 * Created by  gulsh on 1/12/2019.
 */
public class ThreadProducerConsumerExample extends Object {

    boolean workDoneFlag = false;

    public static String getThreadName() {
        return Thread.currentThread().getName() + " ";
    }

    public static void main(String args[]) {
        new ThreadProducerConsumerExample().testForWait();
    }

    public void testForWait() {
        MessageStr messageStr = new MessageStr("First message");

        SenderThread senderThread = new SenderThread(messageStr);
        senderThread.start();

        ReceiverThread receiverThread = new ReceiverThread(messageStr);
        receiverThread.start();

    }
}


class SenderThread extends Thread {
    MessageStr messageStr;
    String [] msgList = {"1 msg","2 msg","3 msg"};
    public SenderThread(MessageStr messageStr) {
        this.messageStr = messageStr;
    }

    @Override
    public void run() {

        for (String msg : msgList) {
            if(messageStr.getMsg() != null){
                synchronized (messageStr){
                    try {
                        messageStr.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.messageStr.setMsg(msg);
            synchronized (this.messageStr){
                this.messageStr.notifyAll();
            }
        }
    }
}

class ReceiverThread extends Thread {
    MessageStr messageStr;

    public ReceiverThread(MessageStr messageStr) {
        this.messageStr = messageStr;
    }

    @Override
    public void run() {

        while(true){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(this.messageStr.getMsg() == null)
                synchronized (this.messageStr){
                    try {
                        this.messageStr.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            System.out.println(this.messageStr.getMsg());
            this.messageStr.setMsg(null);
            synchronized (this.messageStr){
                this.messageStr.notifyAll();
            }
        }
    }
}

class MessageStr {
    String msg;

    public MessageStr(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
