package com.lufick.javatest;

import java.util.ArrayList;

/**
 * Created by  gulsh on 1/10/2019.
 */
public class CollectionExample {

    public static ArrayList<Names> stringArrayList = new ArrayList<>();



    public static void main(String args[]) {

        stringArrayList.add(new Names("Rajat", "rajat@"));
        stringArrayList.add(new Names("Roshan", "roshan@"));
        stringArrayList.add(new Names("Rajnish", "Rajnish@"));
        stringArrayList.add(new Names("Shweta", "shweta@"));
        stringArrayList.add(new Names("Gulshan", "gulshan@"));
        stringArrayList.add(new Names("A", "gulshan@"));
        for (Names name :
                stringArrayList) {
            System.out.println(name);
        }



        //Collections.sort(stringArrayList);

        /*for (Names name :
                stringArrayList) {
            System.out.println(name);
        }*/

    }

    /*public static class NamesSorting implements Comparator<Names> {

        @Override
        public int compare(Names o1, Names o2) {
            return o1.name.compareTo(o2.name);
        }
    }*/

    public static class Names implements Comparable<Names> {
        String name;
        String email;

        Names(String name, String email) {
            this.name = name;
            this.email = email;
        }


        @Override
        public String toString() {
            return name + ":" + email;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Names names = (Names) o;

            if (!name.equals(names.name)) return false;
            return email.equals(names.email);
        }

        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 99 * result + email.hashCode();
            return result;
        }

        @Override
        public int compareTo(Names o) {
            return this.name.compareTo(o.name);
        }
    }


}
