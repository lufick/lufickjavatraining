package com.lufick.javatest;

/**
 * Created by  gulsh on 1/12/2019.
 */
public class AndroidThreading {

    public static int counter = 0;
    public static void main(String args[]) throws InterruptedException {


        Thread thread1 = new CounterThread();
        thread1.setName("F_Thread");
        thread1.start();

        Thread.sleep(1000);

        Thread thread2 = new CounterThread();
        thread2.setName("S_Thread");
        thread2.start();

        Thread.sleep(1000);

        Thread thread3 = new CounterThread();
        thread3.setName("T_Thread");
        thread3.start();

    }

}


class CounterThread extends Thread{
    @Override
    public void run() {
        for(int i=0;i<10;i++){
            ++AndroidThreading.counter;
            System.out.println(Thread.currentThread().getName()+" = "+AndroidThreading.counter);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(Thread.currentThread().getName().equals("F_Thread")){
                System.out.println("------------");
            }
        }
    }
}
