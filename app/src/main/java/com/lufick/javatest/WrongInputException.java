package com.lufick.javatest;

/**
 * Created by  gulsh on 1/8/2019.
 */
public class WrongInputException extends Exception {

    int code;
    String detailMsg;

    WrongInputException(String msg,int code, String detailMsg,Exception e) {
        super(msg,e);
        this.code = code;
        this.detailMsg = detailMsg;
    }

}
