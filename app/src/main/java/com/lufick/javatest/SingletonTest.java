package com.lufick.javatest;

/**
 * Created by  gulsh on 1/12/2019.
 */
public class SingletonTest {

    int inc;
    int dec;
    private static SingletonTest singletonTest;

    Object lock = new Object();
    Object lock1 = new Object();

    private SingletonTest() {
        System.out.println("Creating Object");
    }

    public synchronized static SingletonTest getInstance() {
        if (singletonTest == null) {
            singletonTest = new SingletonTest();
        }
        return singletonTest;
    }


    public void increment() {
        synchronized (lock) {
            inc = inc + 1;
        }
    }

    public void decrement() {
        synchronized (lock1) {
            dec = dec - 1;
        }
    }

}
