package com.lufick.javatest.FileSystem;

import java.util.ArrayList;

/**
 * Created by  gulsh on 1/8/2019.
 */
public abstract class FileSystem {

    public abstract ArrayList<String> listFiles() throws Exception;
}
