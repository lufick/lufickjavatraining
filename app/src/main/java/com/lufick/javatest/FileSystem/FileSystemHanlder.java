package com.lufick.javatest.FileSystem;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by  gulsh on 1/8/2019.
 */
public class FileSystemHanlder {
    public static void main(String args[]) {

        System.out.println("Enter code for FIleSystem");
        Scanner scanner = new Scanner(System.in);
        int code = scanner.nextInt();

        FileSystem fileSystem;
        if (code == 0) {
            fileSystem = new DriveFileSystem();
        } else {
            fileSystem = new DropBoxFileSystem();
        }

        try {
            ArrayList<String> stringArrayList = fileSystem.listFiles();
            for (String name : stringArrayList) {
                System.out.println(name);
            }
        } catch (FSException.AuthException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
