package com.lufick.javatest.FileSystem;

import java.util.ArrayList;

/**
 * Created by  gulsh on 1/8/2019.
 */
public class DriveFileSystem extends FileSystem {
    @Override
    public ArrayList<String> listFiles() throws Exception {
        try{
            if (true)
                throw new DriveAuthException();
            ArrayList<String> fileName = new ArrayList<>();
            fileName.add("Drive New Folder");
            fileName.add("Drive New Folder1");
            fileName.add("Drive New Folder2");
            fileName.add("Drive New Folder3");
            fileName.add("Drive New Folder4");
            return fileName;
        }catch (Exception e){
            throw converException(e);
        }
    }

    static class DriveAuthException extends Exception {

    }


    public FSException.AuthException converException(Exception e){
        if(e instanceof DriveAuthException){
            return new FSException.AuthException();
        }
        return null;
    }
}


