package com.lufick.javatest.FileSystem;

import java.util.ArrayList;

/**
 * Created by  gulsh on 1/8/2019.
 */
public class DropBoxFileSystem extends FileSystem {
    @Override
    public ArrayList<String> listFiles() throws Exception {
        if(true)
            throw new DropBoxAuthException();
        ArrayList<String> fileName = new ArrayList<>();
        fileName.add("DropBox New Folder");
        fileName.add("DropBox New Folder1");
        fileName.add("DropBox New Folder2");
        fileName.add("DropBox New Folder3");
        fileName.add("DropBox New Folder4");
        return fileName;
    }


    static class DropBoxAuthException extends Exception{

    }
}
