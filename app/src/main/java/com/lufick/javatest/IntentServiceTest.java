package com.lufick.javatest;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by  gulsh on 1/12/2019.
 */
public class IntentServiceTest extends IntentService {

    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;
    String processId = "compress-01";
    String errorId = "compress-02";
    String channelName = "CompressService";
    int notId = 121345;


    /*@Subscribe()
    public void onEvent(EventUpdate update) {
        if (System.currentTimeMillis() - this.lastUpdateNotification > 250) {
            this.mNotificationManager.notify(notId,
                    getUpdateNotifiation(
                            update.id,
                            update.dest,
                            getTitle(this, update),
                            getSubTitle(this, update), update.totalProgress));
            this.lastUpdateNotification = System.currentTimeMillis();
        }
    }*/

    private Notification getUpdateNotifiation(String title, String subtitle, int totalProgress) {

        Intent intent = new Intent(this, ThreadActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    processId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        this.mBuilder.setSmallIcon(R.mipmap.ic_launcher).
                setContentTitle(title)
                .setContentText(subtitle)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setProgress(100, totalProgress, false);
        this.mBuilder.setContentIntent(resultPendingIntent);
        return this.mBuilder.build();
    }


    public IntentServiceTest() {
        super("My_Service");
    }



    @Override
    protected void onHandleIntent(Intent intent) {

        startForeground(notId, getUpdateNotifiation("Working","More Working",100));

        String souorcePath = intent.getStringExtra(ThreadActivity.SOURCE_PATH);
        String destPath = intent.getStringExtra(ThreadActivity.DEST_PATH);
        //
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mNotificationManager.notify(notId,getUpdateNotifiation("Working","More Working",i*10));
            Log.e("THREAD", "My Intent service is running..");
        }

        ArrayList<String> names = new ArrayList<>();
        names.add("a");
        names.add("b");
        names.add("c");
        EventBus.getDefault().post(new EventMSG.MSGStr("Unable to process request",names));

        Log.e("THREAD", "Finished");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.mBuilder = new NotificationCompat.Builder(this, processId);
        this.mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mNotificationManager.cancel(notId);
    }
}
