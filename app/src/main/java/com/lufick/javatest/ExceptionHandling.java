package com.lufick.javatest;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by  gulsh on 1/8/2019.
 */
public class ExceptionHandling {

    static PrintWriter printWriter;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void main(String args[]) throws Exception{


        try{
            throw new RuntimeException("Wrong Input Exception");
        }catch (Exception e){

            throw new WrongInputException("Failed to connect",101,"inputs wrong",e);

        }





    }

    public static String getTextExample(int counter) throws IOException {
        return getText(counter);
    }

    public static String getText(int counter) throws IOException {

        return "Hello World " + counter + "\n";
    }


}
