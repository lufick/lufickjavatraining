package com.lufick.javatest;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {


    ArrayList<User> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        names.add(new User("Ram", "21", "12312334234"));
        names.add(new User("A", "21", "12312334234"));
        names.add(new User("Shyam", "21", "12312334234"));
        names.add(new User("John", "21", "12312334234"));
        names.add(new User("Viky", "21", "12312334234"));
        names.add(new User("Brajesh", "21", "12312334234"));
        names.add(new User("Vivek", "21", "12312334234"));
        names.add(new User("Test", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));
        names.add(new User("Rahim", "21", "12312334234"));


        RecyclerView myList = findViewById(R.id.my_list);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        myList.setLayoutManager(linearLayoutManager);
        final MyAdapter myAdapter = new MyAdapter(names);
        myList.setAdapter(myAdapter);


        final Button deleteButton = findViewById(R.id.delete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Iterator<User> userIterable = names.iterator();
                int pos = 0;
                while (userIterable.hasNext()) {

                    User user = userIterable.next();
                    if (user.isSelected()) {
                        userIterable.remove();
                        //myAdapter.notifyItemRemoved(pos);
                        myAdapter.notifyDataSetChanged();
                    }
                    pos++;
                }

               /* ArrayList<User> positions = new ArrayList<>();


                int deletedCount = 0;
                for (int i = 0; i < names.size(); i++) {
                    User user = names.get(i);
                    if (user.isSelected()) {

                        //int pos = names.indexOf(user);
                        names.remove(user);
                        myAdapter.notifyDataSetChanged();
                        //myAdapter.notifyItemRemoved(pos);
                        deletedCount++;
                    }
                }

                Toast.makeText(MainActivity.this, "Deleted " + deletedCount, Toast.LENGTH_SHORT).show();*/
            }
        });
    }
}

class User {
    String name;
    String age;
    String mobile;
    boolean selected;

    public User(String name, String age, String mobile) {
        this.name = name;
        this.age = age;
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}


class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<User> userArrayList;


    public MyAdapter(ArrayList<User> userArrayList) {
        this.userArrayList = userArrayList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView age;
        TextView mobile;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            age = itemView.findViewById(R.id.age);
            mobile = itemView.findViewById(R.id.mobile);
        }
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyAdapter.MyViewHolder viewHolder, int i) {

        final User user = userArrayList.get(viewHolder.getAdapterPosition());

        viewHolder.name.setText(userArrayList.get(viewHolder.getAdapterPosition()).getName());
        viewHolder.age.setText(userArrayList.get(viewHolder.getAdapterPosition()).getAge());
        viewHolder.mobile.setText(userArrayList.get(viewHolder.getAdapterPosition()).getMobile());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(viewHolder.itemView.getContext(), user.getName(), Toast.LENGTH_LONG).show();
                boolean flag = user.isSelected();
                user.setSelected(!flag);
                notifyItemChanged(viewHolder.getAdapterPosition());
            }
        });

        if (user.isSelected()) {
            viewHolder.itemView.setBackgroundColor(Color.parseColor("#80DEEA"));
        } else {
            viewHolder.itemView.setBackgroundColor(Color.WHITE);
        }

    }

    @Override
    public int getItemCount() {
        return userArrayList.size();
    }
}
