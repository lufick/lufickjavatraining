package com.lufick.javatest;


public class ThreadExample {

    public static void main(String args[]){

        System.out.println("Process started");

        SingletonTest singletonTest = SingletonTest.getInstance();

        Thread downloaderThread = new Thread(new MyThread());
        downloaderThread.setName("My Thread: ");
        downloaderThread.setPriority(Thread.MAX_PRIORITY);
        downloaderThread.start();


        /*
        Thread downloaderThread1 = new Thread(new MyThread());
        downloaderThread1.setName("My Thread2: ");
        downloaderThread1.setPriority(Thread.MIN_PRIORITY);
        downloaderThread1.start();*/

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        //System.out.println(singletonTest.getName());
        //SingletonTest singletonTes1 = SingletonTest.getInstance();
        //SingletonTest singletonTes2 = SingletonTest.getInstance();

    }





    public static void download(){
        //System.out.println(SingletonTest.getInstance().getAddress());
        /*for(int i=0;i<5;i++){
            *//*try {
                Thread.sleep(TimeUnit.MILLISECONDS.convert(1 ,TimeUnit.SECONDS));
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }*//*
            if(Thread.interrupted()){
                break;
            }
            String name = Thread.currentThread().getName();
            System.out.println(name+" Thread Downloading...");
        }*/
    }

    public static void upload(){
        for(int i=0;i<5;i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String name = Thread.currentThread().getName();
            System.out.println(name+" Thread Uploading...");
        }
    }

}

class MyThread implements Runnable{
    @Override
    public void run() {
        //super.run();
        ThreadExample.download();

    }
}
