package com.lufick.javatest;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.Callable;

import bolts.Task;

public class ThreadActivity extends AppCompatActivity {

    public static final String SOURCE_PATH = "SOURCE_PATH";
    public static final String DEST_PATH = "DEST_PATH";

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageReceive(EventMSG.MSGStr msgStr){
        Toast.makeText(this, "Message "+msgStr.msg+" | Size:"+msgStr.names.size(), Toast.LENGTH_SHORT).show();
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);

        /*checkNetworkConnection().continueWith(new Continuation<Boolean, Boolean>() {
            @Override
            public Boolean then(Task<Boolean> task) throws Exception {
                if(task.isCompleted()){
                    //Handle error
                    Toast.makeText(ThreadActivity.this, "Error "+task.getError().getCause().getMessage(), Toast.LENGTH_LONG).show();
                } else {

                }
                return null;
            }
        },Task.UI_THREAD_EXECUTOR);

        checkNetworkConnection().continueWith(new Continuation<Boolean, Object>() {
            @Override
            public Object then(Task<Boolean> task) throws Exception {
                return null;
            }
        });*/



        /*AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ThreadActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ThreadActivity.this, "Toast from background", Toast.LENGTH_LONG).show();
                    }
                });
                //Log.e("THREAD","Small Async task "+Thread.currentThread().getName());
            }
        });*/

        /*final MyAsyncTask1 myAsyncTask1 = new MyAsyncTask1();
        myAsyncTask1.execute("Arg1", "Arg2");
        //myAsyncTask1.cancel(true);

        */

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ThreadActivity.this, IntentServiceTest.class);
                intent.putExtra(SOURCE_PATH,"asdfasfsdaf");
                intent.putExtra(DEST_PATH,"destpath");

                startService(intent);
            }
        });

    }

    public Task<Boolean> checkNetworkConnection() {
        return Task.callInBackground(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Thread.sleep(2000);
                return true;
                // return null;
            }
        });
    }

    class MyAsyncTask1 extends AsyncTask<String, Integer, Boolean> {

        Exception caughtException;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            try {
                for (int i = 0; i < 10; i++) {
                    if (isCancelled())
                        break;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    publishProgress(i);

                    if (i > 5) {
                        throw new RuntimeException("My Exception");
                    }
                    Log.e("THREAD", "do in background " + i);
                }
            } catch (Exception e) {
                caughtException = e;
                return false;
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.e("THREAD", "Progress " + values[0]);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if (aBoolean) {
                Log.e("THREAD", "onPostExecute " + aBoolean);
            } else {
                if (caughtException != null)
                    Toast.makeText(ThreadActivity.this, "Unable to process request " + caughtException.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Log.e("THREAD", "onCancelled ");
        }
    }
}
