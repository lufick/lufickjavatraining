package com.lufick.basic;

/**
 * Created by  gulsh on 3/19/2019.
 */
public class JavaError {


    public static void main(String[] args) {

        Singleton s1 = Singleton.getObject();
        Singleton s2 = Singleton.getObject();

    }
}

class Singleton {
    int singleVar = 0;

    private static Singleton singleton;

    private Singleton() {

    }

    public synchronized static Singleton getObject() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }
}
