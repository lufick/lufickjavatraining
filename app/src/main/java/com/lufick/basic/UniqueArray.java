package com.lufick.basic;

/**
 * Created by  gulsh on 3/1/2019.
 */
public class UniqueArray {

    static String names[] = new String[100];
    static int insertedPosition = 0;


    public static void main(String args[]) {

        String text = "Googlefsdfasdfsafd";
        String result = "";
        for (int i = 0; i < text.length(); i++) {
            char charAt = text.charAt(i);
            if (result.toLowerCase().contains(String.valueOf(charAt).toLowerCase())) {
            } else {
                result = result + String.valueOf(charAt);
            }
        }

        System.out.println("result : " + result);
    }



    /*public static void main(String args[]) {

        addName("Ram");
        addName("Ram");
        addName("Ram");
        addName("Ram");
        addName("Ram");
        addName("Ram");
        addName("Ram");
        addName("Ram");
        addName("Shayam");
        addName("Shayam1");
        addName("Shayam2");
        System.out.println("All array values");
        for (int i = 0; i < names.length; i++) {
            if (names[i] == null)
                break;
            System.out.println(names[i]);
        }
    }*/


    private static void addName(String name) {

        if (insertedPosition == 0) {
            names[insertedPosition] = name;
            ++insertedPosition;
            return;
        }


        boolean isFound = false;
        for (int i = 0; i < insertedPosition; i++) {
            if (names[i] == null)
                continue;
            if (names[i].equals(name)) {
                isFound = true;
                break;
            }
        }
        if (isFound) {
            //Do not add
        } else {
            names[insertedPosition] = name;
            ++insertedPosition;
        }
    }
}
