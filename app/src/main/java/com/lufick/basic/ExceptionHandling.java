package com.lufick.basic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by  gulsh on 1/29/2019.
 */
public class ExceptionHandling {

    public static void main(String [] args){


        try(
                InputStream inputStream = new FileInputStream("C://dasfafd");
                InputStream inputStream1 = new FileInputStream("C://dasfafd");
                MyStream myStream = new MyStream();
        ) {

            inputStream.read();

            //inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class MyStream implements AutoCloseable {


    @Override
    public void close() {

    }
}