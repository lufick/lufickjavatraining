package com.lufick.basic;


public class Constructor {
    public static void main(String args[]) {

        Baleno b = new Baleno();
        b.wheels();

    }
}


class Car {
    public int wheels() {
        return 4;
    }
}

class Ciaz extends Car {

}

class Baleno extends Car {
    @Override
    public int wheels() {
        //
        int wheels = super.wheels();
        //apply allowwheel
        return wheels;
    }
}